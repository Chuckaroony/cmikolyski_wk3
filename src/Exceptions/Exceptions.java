package Exceptions;

import java.util.*;

public class Exceptions {

    // Divider method to perform division equation.
    public static float Divider ( float numOne, float numTwo)throws ArithmeticException {
        float quotient;
        //I use an if statement to check if the input is zero. If it's not zero, the division
        // is performed and the answer returned. Any other situation will throw a new
        //Arithmetic Exception, which would include a string being passed. However, if the incorrect data type
        //is entered, the program should have caught it already.
        if (numTwo !=0) {
            quotient = numOne / numTwo;
            return quotient;
        } else {
            throw new ArithmeticException("Please don't use zero as the denominator.");
        }
    }
    // main method for Exceptions Class.
    public static void main (String[]args){
        float answer;
        float firstInput = 0;
        float secondInput = 0;
        Scanner keyBoard = new Scanner(System.in);
        boolean run= true;
        boolean runTwo = true;

        //I use this loop to perform the Divider method, display it's output, and catch it's exception that Divider throws.

        do {
            //I use another do while loop so the exceptions thrown from getting the user input is independent of the
            //Divider method
            do {
                try {
                    //This is where I get the user input. Putting it in the try portion of the try catch block makes it possible
                    //to catch the exception that is thrown if a user enters something other than a number.
                    System.out.println("Please enter any number for the numerator of division equation:");
                    firstInput = keyBoard.nextFloat();
                    System.out.println("Please enter any number for the denominator of division equation:");
                    secondInput = keyBoard.nextFloat();
                    runTwo = false;

                    //This catch statement catches the InputMismatchException that is thrown if the user input
                    //is anything other than a number.
                } catch (InputMismatchException ime) {
                    System.out.println(ime + " Please enter a number only.");
                    keyBoard.next();
                }
            } while (runTwo);

            //This is try catch block for the Divider method which performs the division equation.
            try {
                answer = Divider(firstInput, secondInput);
                System.out.println(firstInput + " / " + secondInput + " = " + answer );
                run= false;
            }


            //This catch statement catches the ArithmeticException thrown
            // from the Divider method and then displays to the screen the user friendly
            //error message created in the divider method.
            catch (ArithmeticException ae) {
                System.out.println(ae);
            }

            //I include this final statement because the instructions say to display some text in the final statement.
            finally {
                System.out.println("Isn't this amazing?");
            }
        } while (run);

    }
}


