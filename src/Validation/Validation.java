package Validation;

import java.util.Scanner;

public class Validation {

    //This is the method I use to check and see if the user input is zero. It calls the GetInput method.
    // With a custom message passed as a parameter to the GitInput.
    public static float ZeroCheck() {
        boolean run = true;
        float theNum;
        //This do while loop makes sure that get input is called again and the number validated as a non zero.
        do {
            theNum = GetInput("Please enter a number for the denominator.");
            if (theNum != 0) {
                run = false;
            }
            else {
                System.out.println("Please don't use a zero.");
            }
        } while (run);
        return theNum;
    }

    //This is the method used to get the user input and make sure that it is a number.
    // It returns the user input if it is in fact a number.
    public static float GetInput(String msg) {
       Scanner keyBoard = new Scanner(System.in);
        boolean run = true;
        //This do while block gives the user the option to enter another number
        //the user input is not a number. Users can also break out of the loop and
        //close the program if so desired.
        do {
            System.out.println(msg + " Enter quit to exit.");
            if (!keyBoard.hasNextFloat()){
                if (keyBoard.hasNextLine()) {
                    String quit = keyBoard.nextLine();
                    if (quit.equalsIgnoreCase("quit")){
                        System.out.println("Good bye!");
                        System.exit(0);
                    }
                } else {
                    keyBoard.next();
                }
            }
            else {
                run = false;
            }
        }while (run);
        return keyBoard.nextFloat();
    }

    //Divider method to perform the division equation.
    //In this class the Divider method does not throw any exception because the numbers have
    // already been validated before being passed to this method.
    public static float Divider ( float numOne, float numTwo) {
        float quotient = numOne / numTwo;
        return quotient;
    }
    //This is the main method. It calls all the methods to perform the validation and displays the result.
    public static void main (String[]args){
        float firstInput = GetInput("Please enter a number for the numerator.");
        float secondInput = ZeroCheck();
        float answer = Divider(firstInput, secondInput);
        System.out.println(firstInput + " / " + secondInput + " = " + answer );


    }
}


